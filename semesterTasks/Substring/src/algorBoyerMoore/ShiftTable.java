package algorBoyerMoore;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

/**This is a functional interface with a single default method and a single
 * abstract method.
 * @author Enikeev Artur.
 * */
public interface ShiftTable {

    /**This is the default method of this interface which generating alphabet of
     * String line using a template's symbols if the line does not contains their.
     * Also alphabet is like a set of symbols in a byte form for economy memory.
     * @param strBytes is a byte array of String line.
     * @param template is a byte array of searching substring.
     * @return alphabet as a byte array.
     * */
    default byte[] generateAlphabet(byte[] strBytes, byte[] template){
        ArrayList<Byte> alphabet = new ArrayList<>();
        for (byte b: strBytes){
            if (alphabet.contains(b)) continue;
            alphabet.add(b);
        }
        for (byte b: template){
            if (alphabet.contains(b)) continue;
            alphabet.add(b);
        }
        byte[] result = new byte[alphabet.size()];
        Iterator<Byte> iter = alphabet.iterator();
        int ind = 0;
        while (iter.hasNext()){
            result[ind] = iter.next();
            ind++;
        }
        return result;
    }
    /**This is an abstract method for generating Shift table.
     * @param template is a byte array of searching substring.
     * @param alphabet is a byte array of symbols of set.
     * @return the shift table for this searching substring and this alphabet
     * as a HashMap of Byte keys and Integer values.*/
    public HashMap<Byte,Integer> generateTable(byte[] template,byte[] alphabet);
}
