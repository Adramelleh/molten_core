package algorBoyerMoore;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

/**This is a class which do a main work of the program, but does not
 * contain a main method. The meaning of the work this class doing is in
 * a searching a user's substring in a text read from a default file or
 * user's file.
 * @author Enikeev Artur.
 * */
public class BoyerMoore implements ShiftTable{

    /**This field holds the full path to the file.*/
    private final File file;

    /**This is an array of indexes of all positions of user's substring in the file.*/
    private int[] indexes;

    /**This is a user's substring in a byte array format.*/
    private byte[] template;

    /**Creating an object of this class.
     * @param path is a full path to the file.
     * @param template is a user's searching substring
     * @throws java.io.FileNotFoundException if file does not exists.*/
    public BoyerMoore(String path, String template) throws FileNotFoundException{
        file = new File(path);
        if (!file.exists()) throw new FileNotFoundException();
        this.template = template.getBytes();
    }

    /**This is inner method for deleting the first byte in a byte array with
     * a left shift.
     * @param array is a byte array which first byte should to be deleted. */
    private static void delFirsByte(byte[] array){
        System.arraycopy(array,1,array,0,array.length-1);
        array[array.length-1] = 0;
    }

    /**This is overriding of the interface's method. This method creates shift table
     * by next logic: if the table doe not contains researching at the moment symbol
     * it's put into the table, else method skips it.
     * @param template is a byte array format of the user's substring.
     * @param alphabet
     * the alphabet which is similar to set, because of it contains
     *                 symbols (in a byte format) in a single example.
     * @return HashMap of Byte keys and Integer values. Keys are the bytes of the symbols of
     *         the alphabet and of the using at the moment string line if alphabet does not
     *         contain any symbols. Values are the quantity of positions in the line, which the
     *         program should skip in iterations, for each symbol.
     * */
    @Override
    public HashMap<Byte, Integer> generateTable(byte[] template, byte[] alphabet) {
        HashMap<Byte,Integer> table = new HashMap<>();
        for (byte sb: alphabet){
            boolean wasMatch = true;
            int i = 0;
            while(i<template.length){
                if (sb == template[i]){
                    wasMatch = true;
                    int shift = template.length-i-1;
                    table.put(sb,(shift == 0?template.length-1:shift));
                    break;
                }
                wasMatch = false;
                i++;
            }
            if (!wasMatch) table.put(sb,template.length-1);
        }
        return table;
    }

    /**This is a method for searching all entering of user's substring on the user's text file.
     * It reads file into the buffer which capacity is equal to [user's substring length]*2.
     * Buffer is a byte array. After that it comparing text in the buffer with substring and,
     * if text contains last one, method writes the index of a first symbol of substring in this
     * part of the text file. Else it reads [n = number of a positions should been skip for this
     * symbol (this meaning holds in shift table)] symbols and compare their again until the end
     * of file.
     * @throws algorBoyerMoore.BoyerMoore.SubstringNotFoundException if file does not contains
     * user's substring.*/
    public void getIndexes() throws SubstringNotFoundException {
        ArrayList<Integer> indexes = new ArrayList<>();
        byte[] buffer = new byte[32];
        try (FileInputStream in = new FileInputStream(file.getAbsoluteFile())){
            int inIndex = in.read(buffer);
            HashMap<Byte,Integer> table = generateTable(template, generateAlphabet(buffer,template));
            int ind = template.length-1;
            int tInd = template.length-1;
            int length = 0;
            while (ind < buffer.length && ind >= 0){
                if (buffer[ind] == template[tInd]){
                    ind--;
                    tInd--;
                    length++;
                    if (length == template.length){
                        indexes.add(inIndex-(buffer.length-ind)+1);
                        length = 0;
                        ind += template.length+1;
                        tInd = template.length-1;
                    }
                } else {
                    if (ind < buffer.length)
                        ind += table.get(buffer[ind]);
                    tInd = template.length-1;
                    length = 0;
                }
            }
            delFirsByte(buffer);
            inIndex += in.read(buffer,buffer.length-1,1);
            if (!table.containsKey(buffer[buffer.length-1])) table.put(buffer[buffer.length-1],template.length-1);
            ind -= buffer.length-1;
            int count = 1;
            while(inIndex > -1){
                if (count == ind){
                    count = 0;
                    ind = buffer.length-1;
                    while (ind < buffer.length && ind >=0){
                        if (buffer[ind] == template[tInd]){
                            ind--;
                            tInd--;
                            length++;
                            if (length == template.length){
                                indexes.add(inIndex-(buffer.length-ind)+1);
                                length = 0;
                                ind += template.length+1;
                                tInd = template.length-1;
                            }
                        } else {
                            if (ind < buffer.length){
                                if (length != template.length-1)
                                    ind += table.get(buffer[ind]);
                                else ind += template.length;
                            }
                            tInd = template.length-1;
                            length = 0;
                        }
                    }
                    ind -= buffer.length-1;
                }
                delFirsByte(buffer);
                inIndex += in.read(buffer,buffer.length-1,1);
                count++;
                if (!table.containsKey(buffer[buffer.length-1])) table.put(buffer[buffer.length-1],template.length-1);
            }
            if (indexes.size()==0){
                throw new SubstringNotFoundException(new String(template));
            }
            Iterator<Integer> iter = indexes.iterator();
            inIndex = 0;
            this.indexes = new int[indexes.size()];
            while (iter.hasNext()){
                this.indexes[inIndex] = iter.next();
                inIndex++;
            }
        } catch (IOException | SubstringNotFoundException e){
            e.printStackTrace();
        }
    }

    /**This is a method for outputting the field [indexes]. Just outputting.*/
    public void outputIndexes(){
        int count = 0;
        for (int i: indexes){
            System.out.print(i + " ");
            count++;
            if (count == 10) System.out.print("\n");
        }
    }

    /**This is exception which is should been throw if the program will not found
     * user's substring in the text.
     * */
    public class SubstringNotFoundException extends Exception{
        SubstringNotFoundException(String substring){
            System.err.println("The substring [\""+substring+"\"] has not found in this file");
        }
    }
}