package mainPart;


import algorBoyerMoore.BoyerMoore;

import java.util.Scanner;
import java.io.FileNotFoundException;

/**The main class of the program. User writes the full path to a file he wants researching or
 * uses default file (Documentation about Scanner). After that he writes a substring he wants
 * to find, and then the program show all indexes of this substring in this file.*/
public class MainWork {
    public static void main(String[] args) {
        final String DEFAULT = "C://Molten_Core/semesterTasks/Substring/src/algorBoyerMoore/text.txt";
        Scanner scan = new Scanner(System.in);
        System.out.println("Write the full path to the file with which you want working." +
                "If you write DEFAULT, the program will work with a default file.\n" +
                "Path to your file is:\t");
        String path = scan.nextLine();
        System.out.println("Write a template:\t");
        try {
            BoyerMoore bm = new BoyerMoore(path.equals("DEFAULT") ? DEFAULT : path, scan.next());
            bm.getIndexes();
            bm.outputIndexes();
        } catch (FileNotFoundException | BoyerMoore.SubstringNotFoundException e){
            e.printStackTrace();
        }
    }
}
