package realisation;

import interfaces.*;

import java.util.Objects;

public class HashTable<K,V> implements HashTables<K,V>{
    private Entry<?,?>[] table;
    private int size;
    private int capacity;
    private final V DEFAULT_VALUE;
    public HashTable(int initialCapacity, V defVal) {
        if (initialCapacity < 0)
            throw new IllegalArgumentException("Illegal Capacity: "+
                    initialCapacity);
        if (initialCapacity==0)
            initialCapacity = 1;
        table = new Entry<?,?>[initialCapacity];
        capacity = initialCapacity;
        DEFAULT_VALUE = defVal;
    }
    private int getIndex(K key){
        return (key.hashCode()&Integer.MAX_VALUE)%10;
    }
    private void retable(int maxIndex){
        Entry<?,?>[] tab = new Entry<?,?>[maxIndex+1];
        System.arraycopy(table,0,tab,0,table.length);
        table = tab;
        capacity = tab.length;
    }
    @Override
    public boolean empty() {
        return size == 0;
    }
    @Override
    public int size() {
        return size;
    }
    @Override
    public int capacity() {
        return capacity;
    }
    @Override
    public boolean contains(K key) {
        int i = getIndex(key);
        if (table[i] != null){
            for (Entry<?,?> entry = table[i]; entry != null; entry = entry.next)
                if (entry.key.equals(key)) return true;
        }
        return false;
    }
    @Override
    public boolean erase(K key) {
        if (!contains(key))
            return false;
        int i = getIndex(key);
        Entry<?,?> e = table[i];
        for (Entry<?,?> prev = null; e != null; prev = e, e = e.next){
            if (e.key.equals(key)){
                if (prev == null) {
                    table[i] = e.next;
                    return true;
                } else {
                    prev.next = e.next;
                    return true;
                }
            }
        }
        return false;
    }
    @Override
    public boolean insert(K key, V value) {
        if (key == null || value == null) throw new NullPointerException();
        if (contains(key)){
            int hash = key.hashCode();
            int i = getIndex(key);
            if (i > table.length-1){
                retable(i);
                table[i] = new Entry<>(hash,key,value,null);
                return true;
            }
            if (table[i].value.equals(value)) return false;
            else {
                table[i] = new Entry<>(key.hashCode(),key,value,table[i]);
                size++;
                return true;
            }
        } else {
            int i = getIndex(key);
            table[i] = new Entry<>(key.hashCode(),key,value,null);
            size++;
            return true;
        }
    }
    @Override
    public void clear() {
        table = new Entry<?,?>[table.length];
        size = 0;
    }
    @Override
    public void clear(int capacity){
        table = new Entry<?,?>[capacity];
        this.capacity = capacity;
        size = 0;
    }
    @Override
    public void trimToSize(int size) {
        Entry<?,?>[] tab = new Entry<?,?>[size];
        System.arraycopy(table,0,tab,0,tab.length>table.length?table.length:tab.length);
        table = tab;
    }
    @Override
    public void swap(HashTables<K,V> b){
        //if (!getKeyClass().equals(b.getKeyClass())||!getValueClass().equals(b.getValueClass()))
        //    throw new IllegalArgumentException("The key or value type of table ["+b+"] " +
        //            "does not equal with key or value type of table ["+this+"]");
        if (capacity < b.capacity()) trimToSize(b.capacity());
        else b.trimToSize(capacity);
        HashTable<K,V> tab = this;
        clear();
        Sets<K> kSet = b.getKeySet();
        for (int i = 0; i < kSet.size(); i++){
            K key = kSet.get(i);
            V value = b.get(key);
            this.insert(key,value);
        }
        b = tab;
    }
    @Override
    public V get(K key) {
        int hash = key.hashCode();
        int i = getIndex(key);
        if (i > table.length-1){
            retable(i);
            table[i] = new Entry<>(hash,key,DEFAULT_VALUE,null);
            return DEFAULT_VALUE;
        }
        for (Entry<?,?> entry = table[i]; entry != null; entry = entry.next){
            if (entry.hash == hash && entry.key.equals(key)) return (V)entry.value;
        }
        table[i] = new Entry<>(hash,key,DEFAULT_VALUE,table[i]);
        return DEFAULT_VALUE;
    }
    @Override
    public V at(K key) {
        int hash = key.hashCode();
        int i = getIndex(key);
        if (i > table.length-1) throw new NullPointerException("This key ["+key+"] is not exists");
        for (Entry<?,?> entry = table[i]; entry != null; entry = entry.next){
            if (entry.hash == hash && entry.key.equals(key)) return (V)entry.value;
        }
        throw new NullPointerException("This key ["+key+"] is not exists");
    }
    @Override
    public Sets<K> getKeySet(){
        return new KeySet<>();
    }
    private static class Entry<Key, Value> implements interfaces.Entry<Key, Value>{
        final int hash;
        final Key key;
        Value value;
        Entry<?,?> next;
        protected Entry(int hash, Key key, Value value, Entry<?, ?> next) {
            this.hash = hash;
            this.key =  key;
            this.value = value;
            this.next = next;
        }
        @Override
        public Value setValue(Value value) {
            if (value == null) throw new NullPointerException();
            Value oldValue = this.value;
            this.value = value;
            return oldValue;
        }
        @Override
        public Value getValue(){
            return value;
        }
        @Override
        public Key getKey(){
            return key;
        }
        public boolean equals(Object o) {
            if (!(o instanceof interfaces.Entry))
                return false;
            interfaces.Entry<?,?> e = (interfaces.Entry<?,?>)o;
            return (key==null ? e.getKey()==null : key.equals(e.getKey())) &&
                    (value==null ? e.getValue()==null : value.equals(e.getValue()));
        }
        public int hashCode() {
            return hash ^ Objects.hashCode(value);
        }
    }
    private class KeySet<K> implements Sets<K>{
        private K[] valArr;
        private int sizeSet;
        private KeySet(){
            valArr = (K[]) new Object[size];
            int ind = 0;
            for (Entry<?, ?> aTable : table) {
                if (aTable == null) continue;
                for (Entry<?, ?> entry = aTable; entry != null; entry = entry.next) {
                    valArr[ind] = (K) entry.key;
                    ind++;
                    sizeSet++;
                }
            }
        }
        @Override
        public int size() {
            return sizeSet;
        }
        @Override
        public void clear() {
            valArr = (K[]) new Object[sizeSet];
            sizeSet = 0;
        }
        @Override
        public boolean empty() {
            return sizeSet == 0;
        }
        @Override
        public boolean contains(K value) {
            for (int i = 0; i < sizeSet; i++)
                if (valArr[i].equals(value))
                    return true;
            return false;
        }
        @Override
        public K get(int index) {
            if (index >= sizeSet) throw new IllegalArgumentException("The index ["+index+"] more than Set's size");
            return valArr[index];
        }
        @Override
        public boolean add(K value) {
            if (contains(value)) return false;
            if (sizeSet < valArr.length){
                sizeSet++;
                valArr[sizeSet] = value;
                return true;
            } else {
                K[] newArr = (K[]) new Object[(valArr.length << 1)+1];
                System.arraycopy(valArr,0,newArr,0,valArr.length);
                valArr = newArr;
                sizeSet++;
                valArr[sizeSet] = value;
                return true;
            }
        }
        @Override
        public K[] toArray() {
            K[] array = (K[]) new Object[sizeSet];
            System.arraycopy(valArr,0,array,0,sizeSet);
            return array;
        }
        @Override
        public void toArray(K[] array) {
            System.arraycopy(valArr,0,array,0,sizeSet);
        }
        @Override
        public K remove(int index) {
            K removed = valArr[index];
            valArr[index] = null;
            return removed;
        }
    }
}