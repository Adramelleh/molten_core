package realisation;

import interfaces.Sets;

import java.util.Random;

/**This is main class. It is only for checking methods of my HashTable.*/
public class Main {
    public static void main(String[] args) {
        int[] keys = new int[15];
        char[] values = new char[15];
        Random rand = new Random();
        for (int i = 0; i < 15; i++){
            keys[i] = rand.nextInt(125);
            values[i] = (char) rand.nextInt(125);
        }
        HashTable<Integer,Character> tab1 = new HashTable<>(10,'D');
        for (int i = 0; i < 15; i++){
            System.out.println(tab1.insert(keys[i],values[i]));
            keys[i] = rand.nextInt(125);
            values[i] = (char) rand.nextInt(125);
        }
        if (tab1.empty()){
            System.out.println("Table 1 is empty");
            return;
        }
        HashTable<Integer,Character> tab2 = new HashTable<>(12,'T');
        for (int i = 0; i < 15; i++)
            System.out.println(tab2.insert(keys[i],values[i]));
        if (tab2.empty()){
            System.out.println("Table 2 is empty");
            return;
        }
        Sets<Integer> set1 = tab1.getKeySet();
        Sets<Integer> set2 = tab2.getKeySet();
        for (int i = 0; i < set1.size(); i++)
            System.out.print(set1.get(i) + " = "+tab1.get(set1.get(i))+"\t");
        System.out.println();
        for (int i = 0; i < set2.size(); i++)
            System.out.print(set2.get(i) + " = " + tab2.get(set2.get(i)) + "\t");
        tab1.swap(tab2);
        System.out.println(tab1.erase(set1.get(0)));
    }
}
