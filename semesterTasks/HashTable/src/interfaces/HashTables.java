package interfaces;

public interface HashTables<K,V> {
    public void swap(HashTables<K,V> b);
    public void clear();
    public void clear(int capacity);
    public void trimToSize(int size);
    public boolean erase(K key);
    public boolean insert(K key, V value);
    public boolean contains(K key);
    public V get(K key);
    public V at(K key);
    public int size();
    public int capacity();
    public boolean empty();
    public Sets<K> getKeySet();
    default Class getKeyClass(){
        K key = (K) new Object();
        return key.getClass();
    }
    default Class getValueClass(){
        V value = (V) new Object();
        return value.getClass();
    }
}
