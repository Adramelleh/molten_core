package interfaces;

public interface Sets<V> {
    public void clear();
    public void toArray(V[] array);
    public boolean contains(V value);
    public boolean add(V value);
    public boolean empty();
    public int size();
    public V get(int index);
    public V[] toArray();
    public V remove(int index);
}
