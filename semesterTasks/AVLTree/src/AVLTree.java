import java.util.Comparator;

public class AVLTree<E,K> {
    private Node<E> root;
    private Comparator<K> comparator;
    public AVLTree(Comparator<K> c){
        root = null;
        comparator = c;
    }
    private void rightBalancing(Node<E> node){
        Node<E> eNode = node.right;
        Node<E> rNode = node.parent;
        if (eNode.left.hasChild()){
            eNode = eNode.left;
            boolean left = comparator.compare(node.key,rNode.key) == 1;
            if (!left) rNode.right = eNode;
            else rNode.left = eNode;
            if (eNode.left != null){
                eNode.right = node.right;
                node.right.left = null;
                node.right.parent = eNode;
                node.right = eNode.left;
                eNode.left.parent = node;
                eNode.left = node;
                node.parent = eNode;
                eNode.parent = rNode;
            } else {
                node.right.left = eNode.right;
                eNode.right.parent = node.right;
                eNode.right = node.right;
                node.right.parent = eNode;
                node.right = null;
                eNode.left = node;
                node.parent = eNode;
                eNode.parent = rNode;
            }
        } else {
            eNode.left.left = node;
            node.parent = eNode.left;
            node.right = null;
            eNode.parent = rNode;
        }
    }
    private void leftBalancing(Node<E> node){
        Node<E> eNode = node.left;
        Node<E> rNode = node.parent;
        if (eNode.right.hasChild()){
            eNode = eNode.right;
            boolean left = comparator.compare(node.key,rNode.key) == 1;
            if (!left) rNode.left = eNode;
            else rNode.right = eNode;
            if (eNode.right != null){
                eNode.left = node.left;
                node.left.right = null;
                node.left.parent = eNode;
                node.left = eNode.left;
                eNode.right.parent = node;
                eNode.right = node;
                node.parent = eNode;
                eNode.parent = rNode;
            } else {
                node.left.right = eNode.left;
                eNode.left.parent = node.left;
                eNode.left= node.left;
                node.left.parent = eNode;
                node.left = null;
                eNode.right = node;
                node.parent = eNode;
                eNode.parent = rNode;
            }
        } else {
            eNode.right.right = node;
            node.parent = eNode.right;
            node.left = null;
            eNode.parent = rNode;
        }
    }
    private Node<E> get(K key, Path path) throws ElementNotFoundException{
        Node<E> node = path.node;
        while(path.hasNext()){
            if (comparator.compare(node.key,key) == 1){
                path.left = false;
                node = path.next();
            } else if (comparator.compare(node.key,key) == -1){
                path.left = true;
                node = path.next();
            } else return node;
        }
        throw new ElementNotFoundException(key);
    }
    private Node<E> getMin(Node<E> node){
        Path path = new Path(node);
        path.left = true;
        while (path.hasNext()) node = path.next();
        if (node.left != null) node = node.left;
        Node<E> eNode = node;
        node = null;
        return eNode;
    }
    public void add(E value, K key){
        Path path = new Path();
        int comRes;
        while (path.hasNext()){
            comRes = comparator.compare(path.node.key,key);
            if (comRes == 0){
                path.node.value = value;
                return;
            }
            path.left = comRes == -1;
            path.next();
        }
        comRes = comparator.compare(path.node.key,key);
        if (comRes == 0){
            path.node.value = value;
            return;
        }
        path.left = comRes == -1;
        path.next();
        comRes = comparator.compare(path.node.key,key);
        if (comRes == 0){
            path.node.value = value;
        } else if (comRes == -1)
            path.node.left = new Node<>(value,key);
        else path.node.right = new Node<>(value,key);
    }
    public E remove(K key) throws ElementNotFoundException{
        Path path = new Path();
        Node<E> removed = get(key,path);
        Node<E> parent = removed.parent;
        boolean left = (parent.left.equals(removed));
        E value = removed.value;
        removed = getMin(removed.right);
        removed.parent = parent;
        if (left) parent.left = removed;
        else parent.right = removed;
        return value;
    }
    public E get(K key) throws ElementNotFoundException{
        Path path = new Path();
        return get(key,path).value;
    }
    private class Node<T>{
        private K key;
        private T value;
        private Node<T> left;
        private Node<T> right;
        private Node<T> parent;
        Node(T value, K key){
            this.value = value;
            this.key = key;
        }
        Node<T> getProblemNode(Node<T> node){
            Node<T> iNode = node.parent;
            boolean left = iNode.equals(iNode.parent.left);
            iNode = iNode.parent;
            if (left){
                if (iNode.right == null)
                    return iNode;
                else {
                    left = iNode.equals(iNode.parent.left);
                    iNode = iNode.parent;
                    if (left){
                        if (iNode.right == null)
                            return iNode;
                        else return null;
                    } else {
                        if (iNode.left == null)
                            return iNode;
                        else return null;
                    }
                }
            } else {
                if (iNode.left == null)
                    return iNode;
                else {
                    left = iNode.equals(iNode.parent.right);
                    iNode = iNode.parent;
                    if (left){
                        if (iNode.left == null)
                            return iNode;
                        else return null;
                    } else {
                        if (iNode.right == null)
                            return iNode;
                        else return null;
                    }
                }
            }
        }
        boolean hasChild(){
            return (left != null || right != null);
        }
        @Override
        public boolean equals(Object obj) {
            Node<T> node = (Node<T>) obj;
            return key == node.key
                    && value == node.value
                    && left == node.left
                    && right == node.right
                    && parent == node.parent;
        }
    }
    private class Path{
        private Node<E> node;
        private boolean left;
        private Path(){
            this.node = root;
        }
        private Path(Node<E> node){
            this.node = node==null?root:node;
        }
        private Node<E> next(){
            Node<E> eNode = node;
            node = left?node.left:node.right;
            return eNode;
        }
        public void setLeft(boolean b){
            left = b;
        }
        public boolean hasNext(){
            if (left) return node.left != null;
            else return node.right != null;
        }
    }
}
