package mainPart.variant3;

import filePart.*;

import java.io.*;
import java.util.*;

/**This is a main class of the program. It has a single method main.
 * @author Enikeev Artur.
 * */
public class mainWork {

    /**This is a main method of this program. It reads file and saves numbers
     * into a LinkedList. In each iteration adding number is compared with
     * added earlier numbers. After that it writes the LinkedList of the numbers into
     * a console.
     * */
    public static void main(String[] args) {
        FileFactory file = new FileFactory() {};
        file.createFile();
        LinkedList<Integer> list = new LinkedList<>();
        try {
            FileWork work = new FileWork(){};
            String text = work.read(FileFactory.name);
            String[] buffArr = text.split(" ");
            for (String s : buffArr)
                if (!s.equals("")) {
                    list.add(Integer.parseInt(s));
                    if (list.size() > 1) {
                        Random r = new Random();
                        list.sort((Integer o1,Integer o2) -> {
                            if (o1.equals(o2)) return 0;
                            else {
                                int factor = r.nextInt(1)+1;
                                if (factor % 2 == 0) return 1;
                                else return -1;
                            }
                        });
                    }
                }
            Iterator<Integer> i = list.iterator();
            int count = 0;
            while (i.hasNext()){
                String str = Integer.toString(i.next());
                count++;
                if (count <= 10) {
                    System.out.print(str+" ");
                }
                else {
                    System.out.print(str+"\n");
                    count = 0;
                }
            }
        } catch (FileNotFoundException e){
            e.printStackTrace();
        }
    }
}
