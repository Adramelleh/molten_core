package filePart;

import java.io.*;
import java.util.Random;

/**This is a functional interface for creating a file which contains
 * random quantity of integer numbers. If file already exists, it deletes
 * and creates again.
 * @author Enikeev Artur.
 * */
public interface FileFactory {

    /**This is a constant which holds a path to the creating file */
    public final static String name = "C://Molten_Core/semesterTasks/Lists/src/filePart/digits.txt";

    /**This is a method for creating file. If file already exists, it delete
     * file and create its again. It is using random for writing integer numbers
     * into the file:
     *      int size = random.nextInt(26)+1;
     *      int quantity = random.nextInt(26)+1;
     * the param size holds the quantity of lines in the file, param quantity
     * holds quantity of numbers in the line.
     * @throws java.lang.RuntimeException if method caught IOException.
     * */
   default void createFile(){
        Random random = new Random();
        File file = new File(name);
        try {
            if (!file.exists()){
                file.createNewFile();
            } else {
                file.delete();
                file.createNewFile();
            }
            try (PrintWriter out = new PrintWriter(file.getAbsoluteFile())) {
                int size = random.nextInt(26)+1;
                int quantity = random.nextInt(26)+1;

                StringBuilder text = new StringBuilder();
                for (int i = 0; i < quantity; i++) {
                    for (int j = 0; j < size; j++) {
                        text.append(Integer.toString(random.nextInt(101)) + " ");
                        if (j == size - 1) text.append("\n");
                    }
                }
                out.print(text.toString());
            }
        } catch (IOException e){
            throw new RuntimeException(e);
        }
    }
}
