package filePart;

import java.io.*;

/**This is a functional interface for working with a created file. It has two methods:
 * [read] and [exists].
 * @author Enikeev Arturn.
 * */
public interface FileWork {

    /**This is a method for reading file.
     * @param fileName is a full path to the file you want to read.
     * @throws java.io.FileNotFoundException if the file does not exist
     * @throws java.lang.RuntimeException if the method caught
     * java.io.IOException.
     * #return the file into a String format.
     * */
    default String read(String fileName) throws FileNotFoundException{
        exists(fileName);
        StringBuilder text = new StringBuilder();
        try {
            try (BufferedReader in = new BufferedReader(new FileReader(new File(fileName).getAbsoluteFile()))) {
                String s;
                while ((s = in.readLine()) != null)
                    text.append(s);
            }
        } catch (IOException e){
            throw new RuntimeException(e);
        }
        return text.toString();
    }

    /**This method checks the file existing.
     * @throws java.io.FileNotFoundException if file does not exist.
     * */
    default
    void exists(String fileName) throws FileNotFoundException{
        File file = new File(fileName);
        if (!file.exists())
            throw new FileNotFoundException();
    }
}
